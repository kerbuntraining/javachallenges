package webParse;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class MainClass {

    public static void main(String[] args) throws IOException {

        String url = "https://listado.mercadolibre.com.ar/cafetera-express#D[A:cafetera%20express,L:undefined]";
        String searchResults = "ol#searchResults";
        String itemAttribute = "div.rowItem.item.highlighted.item--stack.item--has-row-logo.new";
        String imgAttribute = "div.image-content img";
        String realPrice = "div.price__container del";
        String singlePrice = "div.price__container span";

        String description;
        String price;

        Document d= Jsoup.connect(url).get();
        Elements items = d.select(searchResults);

        for(Element item:items.select(itemAttribute)){
            description = item.select(imgAttribute).attr("alt");
            price = getPrice(singlePrice, realPrice, item);
            showProducts(description, price);
        }


    }

    private static String getPrice(String singlePrice, String realPrice, Element item) {
        String price = item.select(realPrice).text();
        if(price.isEmpty()){
            price = item.select(singlePrice).text();
        }
        return price;
    }

    private static void showProducts(String description, String price) {
        System.out.println(description);
        System.out.println(price);
        System.out.println("***********************************************");
    }
}
